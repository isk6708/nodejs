const db = require("../models");
const Pengguna = db.users;
var router = require("express").Router();
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const passportJWT = require('passport-jwt');
const jwt = require('jsonwebtoken');
// const ExtractJwt = passportJWT.ExtractJwt;
const jwtOptions = {};
// jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
// jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'alamisecretkey';

// var strategy = new LocalStrategy(function verify(username, password, cb) {
//   db.get('SELECT * FROM users WHERE username = ?', [ username ], function(err, user) {
//     if (err) { return cb(err); }
//     if (!user) { return cb(null, false, { message: 'Incorrect username or password.' }); }

//     crypto.pbkdf2(password, user.salt, 310000, 32, 'sha256', function(err, hashedPassword) {
//       if (err) { return cb(err); }
//       if (!crypto.timingSafeEqual(user.hashed_password, hashedPassword)) {
//         return cb(null, false, { message: 'Incorrect username or password.' });
//       }
//       return cb(null, user);
//     });
//   });
// });

// var strategy = new LocalStrategy(function verify(username, password, cb) {
//   // var email = req.body.email;
//   // var password = req.body.password;
  
//   db.sequelize.query(
//       "SELECT * FROM users WHERE email = :email AND password = :password",
//       {
//         replacements: { email: email,password: password },
//         type: db.Sequelize.QueryTypes.SELECT,
//         logging: (...msg) => console.log(msg)
//       }
//     ).then(user => {
      
//       if (user.length == 0) { 
//         return cb(null, false, { message: 'Incorrect username or password.' });

//       }else{
//         // console.log('login Success');
//         return cb(null, user);
//       }
//     })
//     .catch(err => {
//       return cb(null, err);
      
//     });

//   // db.sequelize.query('SELECT * FROM users WHERE username = ?', [ username ], function(err, user) {
//   //   if (err) { return cb(err); }
//   //   if (!user) { return cb(null, false, { message: 'Incorrect username or password.' }); }

//   //   return cb(null, user);
    
//   // });
// });
// passport.use(strategy);
// passport.use(new LocalStrategy(
// //   {
// //   emailField: 'email',
// //   passwordField: 'password',
// // },

// (email,password,done) => {
  
//   Pengguna.findOne({ where: { email: email } })
//   .then(user =>{
//     if (!user) {return done(null,false);}
//     Pengguna.comparePassword(password, user.password, (error, isMatch) =>{
      
//         if (error) throw error;

//         if (isMatch) {
//           return done(null,user);
//         } else {
//           return done(null,false);
//         }
//     }); 
//   }).catch(err =>{
//     return done(err);
//   })
// }));

//passport local
// router.post('/passport-local',
// passport.authenticate('local',{successRedirect:'/pengguna/senarai-pengguna',failureRedirect: '/auth/login',failureFlash:true})
// // ,(req,res)=>{
// //   // console.log('ujian');
// //   res.json({ message: 'ok' });
// //   // res.redirect('logged-user');
// // }
// );



//JWT
router.post('/jwt', (req, res) => {
if (req.body.email && req.body.password) {
    const email = req.body.email;
    const password = req.body.password;
    // console.log(email, password);
  Pengguna.findOne({ where: { email: email } })
  .then(user => {
    

    if (!user) {
        res.status(404).json({ message: 'The user does not exist!' });
    }else{
      // console.log(user);
      Pengguna.comparePassword(password, user.password, (error, isMatch) =>
        {
        if (error) throw error;
        if (isMatch) {
          const payload = { id: user.id };
          const token = jwt.sign(payload, jwtOptions.secretOrKey);
          res.json({ message: 'ok', token });
        } else {
          res.json({ message: 'The password is incorrect!' });
          // res.status(401).json({ message: 'The password is incorrect!'
        // });
        }
        });
    } 
  });

}
});

//authenticate user
router.post('/pengesahan', function(req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    
    db.sequelize.query(
        "SELECT * FROM pengguna WHERE email = :email AND password = :password",
        {
          replacements: { email: email,password: password },
          type: db.Sequelize.QueryTypes.SELECT,
          logging: (...msg) => console.log(msg)
        }
      ).then(data => {
        // console.log(data.length);
        if (data.length == 0) { 
          // res.cookie("error", "Please enter correct email and Password!", { httpOnly: true });
          // res.redirect('/auth/login');
          console.log('login failed');
          res.end();
        }else{
          console.log('login Success');
          // res.clearCookie("error", { httpOnly: true });
          // res.cookie("username", data[0].username, { httpOnly: true });
          // res.redirect('/home');
        }
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Pengguna."
        });
        
      });
});

//display login page
router.get('/login', function(req, res, next){ 
    
    res.clearCookie("username", { httpOnly: true });
    const error = req.cookies["error"] || ''; 

    if (error) 
    res.end(`Kita akan bangunkan UI menggunakan Vue utk Login Page Error: ${error}`);
    else  
    // res.end("Kita akan bangunkan UI menggunakan Vue utk Login Page");
    res.render('login');
});

module.exports = router;