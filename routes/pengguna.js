
const pengguna = require("../controllers/userController.js");
const db = require("../models");
const User = db.users;


const passport = require('passport');
var router = require("express").Router();

// const LocalStrategy = require('passport-local').Strategy;

// const passportJWT = require('passport-jwt');
// const jwt = require('jsonwebtoken');
// const ExtractJwt = passportJWT.ExtractJwt;
// const jwtOptions = {};
// jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
// jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
// jwtOptions.secretOrKey = 'alamisecretkey';

// passport.initialize();
// passport.session();

// Retrieve all Pengguna
router.get("/senarai-pengguna",
// (req,res)=>{
//   console.log(req.headers);
//   // console.log(req.headers.authorization);
// }
pengguna.senaraiPengguna
);
// router.get("/senarai-pengguna",isLoggedIn,pengguna.senaraiPengguna);

router.get('/senarai-pengguna-passport',
// passport.authenticate('local',{successRedirect:'/pengguna/senarai-pengguna',failureRedirect: '/auth/login',failureFlash:true}),
passport.authenticate('local',{failureRedirect: '/auth/login',failureFlash:true}),
pengguna.senaraiPengguna
// ,(req,res)=>{
//   // console.log('ujian');
//   res.json({ message: 'ok' });
//   // res.redirect('logged-user');
// }
);


router.get("/senarai-pengguna-jwt", 

passport.authenticate('jwt',{session:false}),
// function(req, res) {
//   res.send(req.user.profile);
// }
pengguna.senaraiPengguna
);

 // Retrieve a single Tutorial with id
 router.get("/:penggunaId", pengguna.cariMengikutId);
  
 
 // Create a new Pengguna
router.post("/create-pengguna", pengguna.ciptaPengguna);
router.post("/create-user", pengguna.createUserJwt);
router.post("/create", pengguna.createUser);

router.get("/email/:email",(req,res) => {
    User.findOne({ where: { email: req.params.email } })
    .then(user => {
    if (!user) {
        res.json({pengguna:false});
    }else{
      res.json({pengguna:true});
    } 
  });
});

// Update a Pengguna with id
router.put("/kemaskini-pengguna/:id", pengguna.kemaskiniPengguna);
router.delete("/hapus-pengguna/:id", pengguna.hapusPengguna);
// router.put("/kemaskini-pengguna/:id", (req,res,next)=>{
//     console.log(req.params);
// });

// router.post('/create-pengguna', function(req, res, next) {
//     console.log(req.body);
//     // res.send('respond with a resource');
//   });

  

module.exports = router;