
const latihan = require("../controllers/latihanController.js");
const passport = require('passport');
var router = require("express").Router();
router.post("/create-latihan", latihan.ciptaLatihan);
router.put("/kemaskini-latihan/:id", latihan.kemaskiniLatihan);
router.delete("/hapus-latihan/:id", latihan.hapusLatihan);
router.get("/senarai-latihan", latihan.senaraiLatihan);
router.get("/senarai-latihan-jwt", passport.authenticate('jwt',{session:false}),latihan.senaraiLatihan);
router.get("/:latihanId", latihan.cariMengikutId);
  
module.exports = router;