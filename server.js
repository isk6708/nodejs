var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require("body-parser");
const cors = require("cors");

const passport = require('passport');
const session = require('express-session');
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require('passport-jwt');
const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;
const jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
// jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'alamisecretkey';


var authRouter = require('./routes/auth');
var penggunaRouter = require('./routes/pengguna');
var latihanRouter = require('./routes/latihan');

var app = express();
var corsOptions = {
  origin: "*"//,"http://127.0.0.1:5173"
};
app.use(cors(corsOptions));

const db = require("./models");
const { log } = require('console');
db.sequelize.sync();
const Pengguna = db.users;

function initialize(passport, carianEmail, getUserById) {
  
    const authenticateUser = async (email, password, done) => {
        Pengguna.findOne({ where: { email: email } })
        .then(user =>{
            if (user == null) {
                return done(null, false, { message: 'No user with that email' })
              }
          
              try {
                Pengguna.comparePassword(password, user.password, (error, isMatch) =>{
                
                        if (error) throw error;
                
                        if (isMatch) {
                          console.log("Match la");
                        } else {
                            console.log("Not Match la");
                        }
                    });
              } catch (e) {
                return done(e)
              }

        }).catch(e =>{
            return done(e)
        });

    //   const user = carianEmail(email)
      
      
    }
  
    passport.use(new LocalStrategy({ usernameField: 'email' }, authenticateUser))
    passport.serializeUser((user, done) => done(null, user.id))
    passport.deserializeUser((id, done) => {
      return done(null, getUserById(id))
    })
  }

function AuthenticateUser(email,password){
    Pengguna.findOne({ where: { email: email } })
    .then(user =>{
        if (user == null){
            // return {message: "Invalid username"};
            console.log("Invalid username");
        }else{
            Pengguna.comparePassword(password, user.password, (error, isMatch) =>{
        
                if (error) throw error;
        
                if (isMatch) {
                  console.log("Match la"+user.id);
                  
                } else {
                    console.log("Not Match la");
                }
            });
            console.log(user.id);
        }
    }).catch(err =>{
        console.log('error'+err);
    })

}
// console.log(AuthenticateUser('isk6708@yahoo.com','12345678'));
passport.use(new JwtStrategy(jwtOptions, function(jwt_payload, done) {
  // console.log(jwt_payload.id);
  Pengguna.findByPk(jwt_payload.id
    , function(err, user) {
    // console/log('hee  lo');
      if (err) {
          return done(err, false);
      }
      // console.log('Hello'+user);
      if (user) {
        // console.log('You are');
          return done(null, user);
      } else {
          return done(null, false);
          // or you could create a new account
      }
  }
  ).then(user=>{
    // console.log(user);
    return done(null, user);
  }).catch(err=>{
    console.log(err);
    return done(null, err);
  })
}));

passport.use(new LocalStrategy(
    {
    usernameField: 'email',
    passwordField: 'password',
  },
  
  (email,password,done) => {
    
    Pengguna.findOne({ where: { email: email } })
    .then(user =>{
      if (!user) {return done(null,false);}
      Pengguna.comparePassword(password, user.password, (error, isMatch) =>{
        
          if (error) throw error;
  
          if (isMatch) {
            return done(null,user);
          } else {
            return done(null,false);
          }
      }).then((user,isMatch)=>{
        if (isMatch) {
          return done(null,user);
        } else {
          return done(null,false);
        }
      })
      .catch(err=>{
        return done(err);  
      }); 
    }).catch(err =>{
      return done(err);
    })
  })
  );

  passport.serializeUser((user, done) => {
    console.log('serialpze');
    done(null, user.id);
  });
  
  passport.deserializeUser((id, done) => {
    console.log('deserial');
    Pengguna.findByPk(id, (err, user) => {
        done(err, user);
    });
  });

// const t = passport.authenticate('local',{successRedirect:'/pengguna/senarai-pengguna',failureRedirect: '/auth/login',failureFlash:true});
// console.log(t);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


app.use(session({
    secret: 'alamisecretkey',//config.SECRET,
    resave: true,
    saveUninitialized: true,
    cookie: { httpOnly: false }
    }))
  
  app.use(passport.initialize());
  app.use(passport.session());

app.use('/auth', authRouter);
app.use('/pengguna', penggunaRouter);
app.use('/latihan', latihanRouter);

app.post('/passport-local',
passport.authenticate('local',{successRedirect:'/pengguna/senarai-pengguna-passport',failureRedirect: '/auth/login',failureFlash:true}),
// passport.authenticate('local',{failureRedirect: '/auth/login',failureFlash:true}),
// pengguna.senaraiPengguna
// ,(req,res)=>{
//   // console.log('ujian');
//   res.json({ message: 'ok' });
//   // res.redirect('logged-user');
// }
);

app.post('/my-login',
  // wrap passport.authenticate call in a middleware function
  function (req, res, next) {
    // console.log('hello');
    // call passport authentication passing the "local" strategy name and a callback function
    passport.authenticate('local', function (error, user, info) {
      // this will execute in any case, even if a passport strategy will find an error
      // log everything to console
      console.log(error);
      console.log(user);
      console.log(info);

      if (error) {
        res.status(401).send(error);
      } else if (!user) {
        res.status(401).send(info);
      } else {
        next();
      }

      res.status(401).send(info);
    })(req, res);
  },

  // function to call once successfully authenticated
  function (req, res) {
    res.status(200).send('logged in!');
  });
  

app.get('/', isLoggedIn,(req, res) => {
    
    // if (isLoggedIn())   {
    //     res.write('Home Logged In'+ req.user.name );
    //     res.end();        
    // }
    res.write('Home Not Logged In' );
        res.end();        
  })

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


app.get('/current_user', isLoggedIn, function(req, res) {
  if(req.user) {
    res.send({ current_user: req.user })
  } else {
    res.status(403).send({ success: false, msg: 'Unauthorized.' });
  }
  })

  function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()){
        console.log('isLoggedIn', req.isAuthenticated());
        return next();
    }   
      
  
  res.redirect('/passport-local');
  console.log('error! auth failed')
  }

  function isNotLoggedIn(req, res, next) {
    if (req.isAuthenticated()){
      res.redirect('/passport-local');
    }
      return next();
  
  
  // console.log('error! auth failed')
  }

  app.post('/logout', function(req, res){
    console.log('logout la');
    // req.logOut();
    // res.send();
    // res.redirect('/auth/login');
  });

//   app.use('/auth', authRouter);
// app.use('/pengguna', penggunaRouter);

PORT = process.env.PORT || 3002;
app.listen(PORT, function() { console.log(`Listening on ${PORT}`) })

module.exports = app;
