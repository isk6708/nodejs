const db = require("../models"); // models path depend on your structure

const bcryptjs = require('bcryptjs');
const Pengguna = db.users;

Pengguna.createUser = (newUser, callback) => {
  bcryptjs.genSalt(10, (err, salt) => {
  bcryptjs.hash(newUser.password, salt, (error, hash) => {
  // store the hashed password
  const newUserResource = newUser;
  newUserResource.password = hash;
  newUserResource.save(callback);
  });
  });
  };

Pengguna.comparePassword = (candidatePassword, hash, callback) => {
  bcryptjs.compare(candidatePassword, hash, (err, isMatch) => {
  if (err) throw err;
  callback(null, isMatch);
  });
  };

  // exports.getUserByEmail = (email, callback) => {
  // const query = { email };
  // Pengguna.findOne(query, callback)
  // .then(user => { 
  //   res.send(user);
  // })
  // };


  exports.createUser = (req, res) => {
    // Validate request
    if (!req.body.username) {
      res.status(400).send({
        message: "Namas Pengguna can not be empty!"
      });
      return;
    }
  
    
  
    // Create a User
    const formInput = {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password
    };
  
    var myuser = Pengguna.build(formInput);
  // console.log(myuser);
  // process.exit(0);
    // Save User in the database
    Pengguna.createUser(myuser,(err,data)=>{
      if (error) { console.log(error); }
      // console.log(data);
      // res.send({ data });
    })
    // .then(data => {
    //     console.log("hello");
    //     // res.send(data);
    //   })
    //   .catch(err => {
    //     res.end(err);
    //     // process.exit(0);
    //     res.status(500).send({
    //       message:
    //         err.message || "Some error occurred while creating the User."
    //     });
    //   });
  };

exports.ciptaPengguna = (req, res) => {
  // Validate request
  if (!req.body.username) {
    res.status(400).send({
      message: "Nama Pengguna can not be empty!"
    });
    return;
  }

  

  // Create a User
  const formInput = {
    username: req.body.username,
    email: req.body.email,
    password: req.body.password
  };

  

  // Save User in the database
  Pengguna.create(formInput)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    });
};

exports.createUserJwt = (req, res) => {
  // Validate request
  if (!req.body.username) {
    res.status(400).send({
      message: "Nama Pengguna can not be empty!"
    });
    return;
  }

  // Create a User
  const formInput = {
    username: req.body.username,
    email: req.body.email,
    password: req.body.password
  };

  function myEncryption(newUser,Model)  {  
    bcryptjs.genSalt(10, (err, salt) => {
      bcryptjs.hash(newUser.password, salt, (error, hash) => {
      const newUserResource = newUser;
      newUserResource.password = hash;
      // return newUserResource;
      Model.create(newUserResource, (error) => {
        if (error) { console.log(error); }
        // res.send({ user });
        })
        .then(data => {
          res.send(data);
        })
        .catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while creating the User."
          });
        });

      });
      });
  };
  
  myEncryption(formInput,Pengguna);
};

exports.senaraiPengguna = (req, res) => {
    const { Op } = require("sequelize");  
    const username = req.query.username;
    var condition = username ? { username: { [Op.like]: `%${username}%` } } : null;
  
    Pengguna.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Pengguna."
        });
      });
  };

  exports.cariMengikutId = (req, res) => {
    const id = req.params.penggunaId;
  
    Pengguna.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Pengguna with id=" + id
        });
      });
  };

  exports.kemaskiniPengguna = (req, res) => {
    const id = req.params.id;
    // console.log(req.body);
    // process.exit(0);
// 20230607 Start
const formInput = {
  username: req.body.username,
  email: req.body.email,
  password: req.body.password
};

function myEncryption(existingUser,Model)  {  
  bcryptjs.genSalt(10, (err, salt) => {
    bcryptjs.hash(existingUser.password, salt, (error, hash) => {
    const existingUserResource = existingUser;
    existingUserResource.password = hash;
    console.log(existingUserResource);
    Model.update(existingUserResource, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Pengguna was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Pengguna with id=${id}. Maybe Pengguna was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Pengguna with id=" + id
        });
      });

    // Model.update(existingUserResource, (error) => {
    //   if (error) { console.log(error); }
    //   // res.send({ user });
    //   })
    //   .then(data => {
    //     res.send(data);
    //   })
    //   .catch(err => {
    //     res.status(500).send({
    //       message:
    //         err.message || "Some error occurred while updating the User."
    //     });
    //   });

    });
    });
};

myEncryption(formInput,Pengguna);

// 20230607 End

    // Pengguna.update(req.body, {
    //   where: { id: id }
    // })
    //   .then(num => {
    //     if (num == 1) {
    //       res.send({
    //         message: "Pengguna was updated successfully."
    //       });
    //     } else {
    //       res.send({
    //         message: `Cannot update Pengguna with id=${id}. Maybe Pengguna was not found or req.body is empty!`
    //       });
    //     }
    //   })
    //   .catch(err => {
    //     res.status(500).send({
    //       message: "Error updating Pengguna with id=" + id
    //     });
    //   });
  };

  exports.hapusPengguna = (req, res) => {
    const id = req.params.id;
  
    Pengguna.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Pengguna was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Pengguna with id=${id}. Maybe Pengguna was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Pengguna with id=" + id
        });
      });
  };