const db = require("../models"); // models path depend on your structure
const Latihan = db.latihan;

exports.ciptaLatihan = (req, res) => {
  // Validate request
  if (!req.body.latihan) {
    res.status(400).send({
      message: "Nama Latihan can not be empty!"
    });
    return;
  }

  // Create a Latihan
  const formInput = {
    latihan: req.body.latihan,
    user_id: req.body.user_id,
    tarikh: req.body.tarikh
  };

  // Save Latihan in the database
  Latihan.create(formInput)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Latihan."
      });
    });
};

exports.senaraiLatihan = (req, res) => {
    const latihan = req.query.latihan;
    var condition = latihan ? { latihan: { [Op.like]: `%${latihan}%` } } : null;
  
    Latihan.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Latihan."
        });
      });
  };

  exports.cariMengikutId = (req, res) => {
    const id = req.params.latihanId;
  
    Latihan.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Latihan with id=" + id
        });
      });
  };

  exports.kemaskiniLatihan = (req, res) => {
    const id = req.params.id;
    // console.log(req.body);
    // process.exit(0);
    Latihan.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Latihan was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Latihan with id=${id}. Maybe Latihan was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Latihan with id=" + id
        });
      });
  };

  exports.hapusLatihan = (req, res) => {
    const id = req.params.id;
  
    Latihan.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Latihan was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Latihan with id=${id}. Maybe Latihan was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Latihan with id=" + id
        });
      });
  };