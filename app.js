var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require("body-parser");
const cors = require("cors");

const jwt = require('jsonwebtoken');
const passport = require('passport');
const session = require('express-session');
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require('passport-jwt');
const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;
const jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
// jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'alamisecretkey';

const db = require("./models");
db.sequelize.sync();
const Pengguna = db.users;
const Bantuan = db.bantuan;

passport.use(new LocalStrategy(
    {
      usernameField: 'email',
    passwordField: 'password',
  },
  
  (email,password,done) => {
    
    Pengguna.findOne({ where: { email: email } })
    .then(user =>{
      if (!user) {return done(null,false);}
      Pengguna.comparePassword(password, user.password, (error, isMatch) =>{
        
          if (error) throw error;
  
          if (isMatch) {
            return done(null,user);
          } else {
            return done(null,false);
          }
      }); 
    }).catch(err =>{
      return done(err);
    })
  }));

  passport.serializeUser((user, done) => {
    done(null, user.id);
  });
  
  passport.deserializeUser((id, done) => {
    Pengguna.findByPk(id, (err, user) => {
        done(err, user);
    });
  });

  
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var penggunaRouter = require('./routes/pengguna');
var latihanRouter = require('./routes/latihan');
var authRouter = require('./routes/auth');

var app = express();

var corsOptions = {
  origin: "*"//,"http://127.0.0.1:5173"
};


app.use(cors(corsOptions));


app.use(session({
  secret: 'alamisecretkey',//config.SECRET,
  resave: true,
  saveUninitialized: true,
  cookie: { httpOnly: false }
  }));

app.use(passport.initialize());
app.use(passport.session());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/home', indexRouter);
// app.use('/users', usersRouter);
app.use('/pengguna', penggunaRouter);
app.use('/latihan', latihanRouter);
app.use('/auth', authRouter);

app.post('/passport-local',
passport.authenticate('local',{successRedirect:'/pengguna/senarai-pengguna',failureRedirect: '/auth/login',failureFlash:true})
// ,(req,res)=>{
//   // console.log('ujian');
//   res.json({ message: 'ok' });
//   // res.redirect('logged-user');
// }
);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});




app.get('/current_user', isLoggedIn, function(req, res) {
  if(req.user) {
    res.send({ current_user: req.user })
  } else {
    res.status(403).send({ success: false, msg: 'Unauthorized.' });
  }
  })

  function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
      return next();
  
  res.redirect('/auth/login');
  console.log('error! auth failed')
  }

  function isNotLoggedIn(req, res, next) {
    if (req.isAuthenticated()){
      res.redirect('/auth/login');
    }
      return next();
  
  
  // console.log('error! auth failed')
  }

  app.delete('/logout', function(req, res){
    // console.log('logout');
    req.logOut();
    // res.send();
    res.redirect('/auth/login');
  });

PORT = process.env.PORT || 3002;
app.listen(PORT, function() { console.log(`Listening on ${PORT}`) })

module.exports = app;
