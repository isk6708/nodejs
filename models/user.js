const bcryptjs = require('bcryptjs');

module.exports = (sequelize, Sequelize) => {

    const User = sequelize.define("user", {
    
    email: {
    type: Sequelize.STRING
    },
    password: {
    type: Sequelize.STRING
    },
    
    username: {
    type: Sequelize.STRING
    }
    
    },
    {
        timestamps: false,
        // tableName: 'pengguna'
    });
    
    
    return User;
    
    };
    // module.exports = User;
    module.exports.getUserByEmail = (email, callback) => {
        const query = { email };
        User.findOne(query, callback);
        };

        module.exports.comparePassword = (candidatePassword, hash, callback) => {
            bcryptjs.compare(candidatePassword, hash, (err, isMatch) => {
            if (err) throw err;
            callback(null, isMatch);
            });
            };

    // module.exports.createUser = (newUser, callback) => {
    //     bcryptjs.genSalt(10, (err, salt) => {
    //     bcryptjs.hash(newUser.password, salt, (error, hash) => {
    //     // store the hashed password
    //     const newUserResource = newUser;
    //     newUserResource.password = hash;
    //     newUserResource.save(callback);
    //     });
    //     });
    //     };