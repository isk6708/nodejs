const dbConfig = require("../dbconfig.js");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});
//----------mysqlj
const sequelizej = new Sequelize(dbConfig.MYSQLJ.DB, dbConfig.MYSQLJ.USER, 
  dbConfig.MYSQLJ.PASSWORD, {
  host: dbConfig.MYSQLJ.HOST,
  dialect: dbConfig.MYSQLJ.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.MYSQLJ.pool.max,
    min: dbConfig.MYSQLJ.pool.min,
    acquire: dbConfig.MYSQLJ.pool.acquire,
    idle: dbConfig.MYSQLJ.pool.idle
  }
});

//---------------mysqlj
//-------------mssql start  
const sequelizex = new Sequelize(dbConfig.MSSQLA.DB, dbConfig.MSSQLA.USER, dbConfig.MSSQLA.PASSWORD, {
    dialect: dbConfig.MSSQLA.dialect,
    host: dbConfig.MSSQLA.HOST,
    dialectOptions: {
        encrypt: false,
        trustServerCertificate: true
    }
});
//-------------mssql end

sequelize.authenticate().then((err) => {
    console.log('Connection successful', err);
})
.catch((err) => {
    console.log('Unable to connect to database', err);
});
// ----------------mssql end

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("./user.js")(sequelize, Sequelize);
// db.bantuan = require("./bantuan.js")(sequelize, Sequelize);
db.latihan = require("./latihan.js")(sequelize, Sequelize);
// db.permohonan = require("./permohonan.js")(sequelize, Sequelize);
// db.department = require("./department.js")(sequelize, Sequelize);
// db.daerah = require("./daerah.js")(sequelize, Sequelize);
// db.tutorials = require("./tutorial.model.js")(sequelize, Sequelize);

module.exports = db;