module.exports = (sequelize, Sequelize) => {

    const Latihan = sequelize.define("latihan", {
    
    user_id: {
    type: Sequelize.INTEGER
    },
    latihan: {
    type: Sequelize.STRING
    },
    
    tarikh: {
    type: Sequelize.DATE
    }
    
    },
    {
        timestamps: false,
        tableName: 'latihan_alami'
    });
    
    
    return Latihan;
    
    };